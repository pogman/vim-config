set shell=/bin/bash
syntax on
set t_Co=256
set tabstop=2 shiftwidth=2 expandtab
set smartindent
set nu!
syntax enable
set background=dark
colorscheme monokai

let g:go_disable_autoinstall = 0
let g:neocomplete#enable_at_startup = 1
autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
map <C-n> :NERDTreeToggle<CR>
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'fatih/vim-go'
Plugin 'Shougo/neocomplete.vim'
Plugin 'dart-lang/dart-vim-plugin'
Plugin 'mattn/emmet-vim'
Plugin 'scrooloose/nerdtree'
Plugin 'vim-scripts/groovy.vim'
Plugin 'elixir-lang/vim-elixir'
Plugin 'msanders/cocoa.vim'
Plugin 'guns/vim-sexp'
Plugin 'dag/vim-fish'
Plugin 'pangloss/vim-javascript'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'keith/swift.vim'
call vundle#end()            " required
filetype plugin indent on
Bundle 'wakatime/vim-wakatime'
if has('vim_starting')
  set nocompatible
  set runtimepath+=~/.vim/bundle/dart-vim-plugin
endif
filetype plugin indent on
